/*=========================================================================

Program:   Logiciel d'enregistrement GigVision
Language:  C++
Date:      21/03/2015
Version:   1.1


Copyright (c) ZENNAYI Yahya® , MAScIR. All rights reserved.

=========================================================================*/
#ifndef GIGE_VISION_CONTROL_H
#define GIGE_VISION_CONTROL_H

/**
 * @file        gigevision_control.h
 * @author      ZENNAYI Yahya, KLILOU Abdessamad
 * @version     1.1
 * @date        21/01/2015
 * @brief
 *
 *
 */
//#define WITH_LOG
#include <QThread>
#include <QTimer>
#include <QDir>
#include <QDateTime>
#include <QMessageBox>
#include <QtXml/QDomDocument>
#include <QTextStream>
#include <QtNetwork>
#include <QInputDialog>
#include <QLineEdit>
#include <QMessageBox>

#ifdef WITH_LOG
//#include "log_file.h"
#endif

#include "GeniCamTypes.h"
/**
  * @enum TypeCamera
  * @brief Le type de la camera utilisé.
  */
typedef enum{
    /// DALSA
    CAMERA_DALSA,
    /// NET
    CAMERA_NET,
    /// POINGRAY
    CAMERA_POINGRAY
}TypeCamera;

#include <QCoreApplication>
#include <opencv2/core/core.hpp>
#include <opencv2/highgui/highgui.hpp>
#include <opencv2/imgproc/imgproc.hpp>


#include "structs.h"

using namespace cv;
using namespace std;


#include "structs.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>



/**
 * @enum ModeCom le mode de communication.
 *
 */
typedef enum{
    /// Mode discovery.
    MODE_DISCOVERY,
    /// Mode initialisation (recupération de fichier xml + initilisation des paramétres de la camera).
    MODE_INITILISE,
    /// Mode lecture d'un paramétre de la camera.
    MODE_CONTROL_READ,
    /// Mode ectruture (Modiffication) d'un paramétre de la camera.
    MODE_CONTROL_WRITE,
    /// Mode d'attante (ecoute et echange de donne pour assurer la communication).
    MODE_SLEEP
}ModeCom;



/**
 * @ingroup     Acquire
 * @class       GigeVisionControl
 * @brief       Le module qui gére la partie controle du protocol GigeVision
 */
class GigeVisionControl : public QThread
{
    Q_OBJECT
public:
    /**
     * @brief Le constructeur.
     * @param parent
     */
    explicit GigeVisionControl(QObject *parent = 0);

    /**
     * @brief Le destructeur.
     */
    ~GigeVisionControl();

    /**
     * @brief Lancer le Discovery pour detecter les cameras
     */
    void LancerDiscovery();

    /**
     * @brief Change l'adresse ip de connection
     * @param adr_ip : la nouvelle adresse IP.
     */
    void ChangeIpAddress(char* adr_ip);

    /**
     * @brief Change l'adresse ip (broadcast) de connection
     * @param adr_ip_broadcast : l'adresse de broadcast.
     */
    void ChangeIpBroadcastAddress(char* adr_ip_broadcast);

    /**
     * @brief Changer l'adresse mac de connection
     * @param adr_mac
     */
    void ChangeMacAddress(char* adr_mac);

    /**
     * @brief recuperer les parametres de la camera
     * @return
     */
    ListGeniCamParam getPramCamera();

    /**
     * @brief Changer la taille des paquets streaming et le delai entre paquet
     * @param size
     * @param delay
     */
    void setSizeDelayPacket(int size,int delay);

    /**
     * @brief Une fonction permetant de lancer l'acuisistion
     * @return
     */
    bool StartAquire_Genicam();

    /**
     * @brief Une fonction permetant d'arretr l'acuisistion
     * @return
     */
    bool StopAquire_Genicam();


    /**
     * @brief Changer la valeur d'un paramétre.
     * @param name : le nom.
     * @param value : la nouvelle valeur.
     */
    void SetParameter(QString name, QString value);

    /**
     * @brief SetParameterInThread
     * @param name
     * @param value
     */
    void SetParameterInThread(QString name, QString value);
    /**
     * @brief Charger la valeur d'un paramétre.
     * @param name : le nom.
     * @return value : la valeur.
     */
    QString GetParameter(QString name);

    void GetParameterInThread(QString name);

    QString GetParameterMax(QString name);
    QString GetParameterMin(QString name);

    /**
     * @brief setType
     * @param type
     */
    void setType(const TypeCamera &type);

signals:

    /**
     * @brief signal de commencer le streaming
     */
    void StartStreaming();
    /**
     * @brief signal d'arreter le streaming
     */
    void StopStreaming();

    /**
     * @brief signal de changement d'etat de connection
     * @param state : le nouveau status
     * @param waiting : le nouveau
     */
    void ConnectionDone(bool state,bool waiting);

    /**
     * @brief Signal qui envoi l'adresse ip destination
     * @param addr
     */
    void SendIpReception(QString addr);

    /**
     * @brief EnabledAquire
     */
    void EnabledAquire(bool);


    /**
     * @brief Envoyer la liste des ip destination detectés
     * @param ip
     */
    void ListIpDestination(QList<QString> ip);

    /**
     * @brief la connexion avec camera OK
     */
    void ConnexionOk();

    /**
     * @brief Change la resolution des images recu
     * @param w : width.
     * @param h : height.
     */
    void ChangedResolution(int w,int h);

    /**
     * @brief Mise ajour de message log.
     * @param status : le nouveau status.
     */
    void UpdatedMessage(QString status);

    /**
     * @brief InitializeHostStreaming
     * @param ip_destination
     * @param port
     */
    void InitializeHostStreaming(QString ip_destination,int port);

    void ParameterChanged(QString name,QString value);
public slots:

       /**
     * @brief tester la connexion
     */
    void TestConnexion(int port);

private slots:
    /**
     * @brief CommunicationSleep
     */
    void CommunicationSleep();

    /**
     * @brief detecteur que le thread est bloqué
     */
    void RunThread();

private :

    /**
     * @brief Arreter la communication
     */
    void Stop();

    /**
     * @brief StartSleep
     */
    void StartSleep();

    /**
     * @brief StopSleep
     */
    void StopSleep();

    /**
     * @brief InitComunication : initialisatiopn de la communication
     * @return
     */
    bool InitCommunication();

    /**
     * @brief Initaliser le serveur
     * @return true : initialisation reussi , false : sinon
     */
    bool Init();

    /**
     * @brief Initialisation avant changement d'un registre
     * @return
     */
    bool InitCommunicationDone();

    /**
     * @brief Lancer le discovry pour assurer la communication
     * @return
     */
    bool Discovry();

    /**
     * @brief Recuperer le fichier xml du camera
     * @return
     */
    bool ReadMemXml();

    /**
     * @brief charger depuis un fichier les paramétres des caméra
     */
    bool LoadParametreCam();

    /**
     * @brief Le thread de communication
     */
    void run();


private :

    /**
     * @brief l image d affichage
     */
    Mat image_display_;

    /**
     * @brief le thread est en execution
     */
    bool runing_;

    /**
     * @brief l initialisation de la camera est OK
     */
    bool initialisation_is_ok_;
    /**
     * @brief Représente le serveur
     */
    QUdpSocket *socket_;

    /**
     * @brief Représente le host (adresse ip, ...)
     */
    QHostAddress host_address_;

    /**
     * @brief le port de connexion
     */
    quint16 port_connexion_;

    /**
     * @brief l'indice du paquet
     */
    int *index_packet_;

    /**
     * @brief l'indice du paquet en tab
     */
    char index_packet_tab_[2];

    /**
     * @brief socket deja initialiser
     */
    bool socket_init_;

    /**
     * @brief ma adresse ip
     */
    QString my_ip_;

    /**
     * @brief l'adresse ip destination
     */
    QString dest_ip_;

    /**
     * @brief l'adresse ip broadcast
     */
    QString ip_broadcast_;

    /**
     * @brief niveau discovry ( xx.xx.xx.255 , xx.xx.255 , xx.255.255.255 )
     */
    int niv_discorery_;

    /**
     * @brief la liste des adresses ip destination probable
     */
    QList<QString> dest_ip_list_;

    /**
     * @brief Le buffer d'acquisition des paquets.
     */
    char RecBuffer[1600];

    /**
     * @brief Le paquet discovery.
     */
    char *discovery_ack_;

    /**
     * @brief timer pour le control de thread
     */
    QTimer *timer_thread_;

    /**
     * @brief Un conteur pour controler le thread (le fonctionnement normal pas de blockage)
     */
    int  last_count_;

    /**
     * @brief Un conteur pour controler le thread (le fonctionnement normal pas de blockage)
     */
    int  next_count_;

    /**
     * @brief mode de communication
     */
    ModeCom mode_com_;

    /**
     * @brief changer un paramétre
     */
    bool change_param_;

    /**
     * @brief activer le changement de valeur de registre par type
     */
    bool change_register_value_by_type_;

    /**
     * @brief la lecture de la valeur d'un parametre a reussi
     */
    bool read_parametre_value_ok_;




    /**
     * @brief le contenu de fichier xml (parametre de communication)
     */
    QString str_file_data_;

    /**
     * @brief un buffer pour stocket le fichier xml compréssé.
     */
    char xml_zip_[500000];

    /**
     * @brief l'indice du fichier xml compréssé.
     */
    int xml_zip_index_;

    /**
     * @brief La longeur du fichir xml.
     */
    int lenght_file_xml;

    /**
     * @brief id des paquet
     */
    int gigvision_id_ ;

    /**
     * @brief lecture de la valeur d'un register est en cours
     */
    bool reading_register_;

    /**
     * @brief l'adresse ip du pc
     */
    char address_ip_pc_[4];

    /**
     * @brief la taille des paquets image
     */
    int packet_size_;

    /**
     * @brief le delai entre paquet
     */
    int packet_delay_;

    /**
     * @brief Afficher les cout pour suivre les erreurs
     */
    bool display_message_error_;

    /**
     * @brief timer pour communiquer en mode sleep
     */
    QTimer *timer_sleep_;

    /**
     * @brief la perdio de steep
     */
    int time_to_steep_;

    /**
     * @brief activer le mode sleep
     */
    bool active_sleep_;

    /**
     * @brief le port de transmission udp streaming
     */
    int udp_port_;

    /**
     * @brief l objet camera genicam
     */
    //CNodeMapRef* camera_;

    void *TL_;

    /**
     * @brief Le type de la camera
     */
    TypeCamera type_;

    /**
     * @brief Attribut qui genere des message log (par serveur)
     */
    //LogFile *log_acquire_;


    QList<QString> name_parameter_to_set_;
    QList<QString> value_parameter_to_set_;
    QList<bool> parameter_to_change_is_set;

    int last_exposurtime_;


    GenApi::CNodeMapRef camera;

};





#endif // GIGE_VISION_CONTROL_H
