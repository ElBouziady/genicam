﻿#ifndef STRUCTS_H
#define STRUCTS_H

#include <QString>
#include <stdint.h>
#include <QMap>

typedef struct{
    int id;
    QString name;
    QString display_name;
    QString type;
    QString value;
    QStringList enum_values;
    QString enum_value;
    QString string_value;
    QString value_max;
    QString value_min;

}GeniCamParam;

class ListGeniCamParam{
    public:
    QMap<QString,GeniCamParam> map_;
};



typedef enum{
    TYPE_RECORDING_SEQUENCE,
    TYPE_RECORDING_VIDEO,
    TYPE_RECORDING_IMAGE

}TypeRecording;



typedef enum{
    RO_,
    RW_

}AccessModeInfoCam;

typedef enum{
    TypeParamInteger,
    TypeParamFloat,
    TypeParamString,
    TypeParamCommand,
    TypeParamEnum,
    TypeParamIntReg

}TypeParamInfoCam;

typedef struct{
    int             id;

    QString         name;

    QString         name_param;

    QString         display_name;

    TypeParamInfoCam type;

    AccessModeInfoCam access_mode;

    QList<QString>         value_s;

    QList<int64_t>        value_int;

    QList<double>          value_float;

    QStringList     value_list;

    bool enabled;

    char addr[4];

    QString pValue;

    int CommandValue;

}ParamInfoCam;


typedef enum{
    UserSetDefaultSelector_enum,

    ExposureMode_enum,

    ExposureAlignment_enum,

    ShadingCorrectionMode_enum,

    TriggerMode_enum,

    TriggerSource_enum,

    TriggerActivation_enum,

    InputLinePolarity_enum,

    PixelFormat_enum,

    TestImageSelector_enum,

    AcquisitionMode_enum,

    ExposureDelay_enum,

    GainAbs_enum,

    TriggerDelayAbs_enum,

    InputLineDebouncingPeriod_enum,

    Width_enum,

    Height_enum,

    OffsetX_enum,

    OffsetY_enum,

    AcquisitionFrameRateAbs

}Feature;

typedef enum{
    Factory,

    UserSet1

}UserSetDefaultSelector;

typedef enum{
    Timed,

    TriggerWidth

}ExposureMode;

typedef enum{
    Reset,

    Synchronous

}ExposureAlignment;

typedef enum{
    Calibration,

    Disable,

    Enable

}ShadingCorrectionMode;

typedef enum{
    Off_T,

    On

}TriggerMode_F;

typedef enum{
    Line1,

    Line2,

    Software

}TriggerSource;

typedef enum{
    FallingEdge,

    RisingEdge

}TriggerActivation;

typedef enum{
    ActiveHigh,

    ActiveLow

}InputLinePolarity;

typedef enum{
    BayerGR10,

    BayerGR8

}PixelFormat;

typedef enum{
    DiagonalMovingWedge,

    HorizontalWedge,

    Off_I,

    Purity,

    VerticalWedge

}TestImageSelector;


typedef enum{
    Continuous,

    MultiFrame,

    SingleFrame

}AcquisitionMode;

union CamVal{
    long long               int_val;

    double                  float_val;

    UserSetDefaultSelector  UserSetDefaultSelector_val;

    ExposureMode            ExposureMode_val;

    ExposureAlignment       ExposureAlignment_val;

    ShadingCorrectionMode   ShadingCorrectionMode_val;

    TriggerMode_F             TriggerMode_val;

    TriggerSource           TriggerSource_val;

    TriggerActivation       TriggerActivation_val;

    InputLinePolarity       InputLinePolarity_val;

    PixelFormat             PixelFormat_val;

    TestImageSelector       TestImageSelector_val;

    AcquisitionMode         AcquisitionMode_val;
};


typedef struct{
    Feature         name;

    CamVal          value;

    int             cam;

}CamInfo;

#endif // STRUCTS_H
