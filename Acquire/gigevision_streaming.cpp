/*=========================================================================

Program:   Logiciel d'enregistrement GigVision
Language:  C++
Date:      21/03/2015
Version:   1.1


Copyright (c) ZENNAYI Yahya® , MAScIR. All rights reserved.

=========================================================================*/

#include "gigevision_streaming.h"


char STM_Image_Leader[44] = { 0x00, 0x00, 0x00, 0x00,  // status && Block_ID
                          0x01, 0x00, 0x00, 0x00,  // pkt_type && packet_ID
                          0x00, 0x00, 0x00, 0x01,  // Rsv & Payload type = Image
                          0x00, 0x00, 0x00, 0x00,  // Time Stamp : High
                          0x07, 0x33, 0x4A, 0x76,  // Time Stamp : Low
                          0x01, 0x08, 0x00, 0x01,  // pixel format
                          0x00, 0x00, 0x05, 0x78,  // x : width
                          0x00, 0x00, 0x04, 0x00,  // y : height
                          0x00, 0x00, 0x00, 0x00,  // offset x
                          0x00, 0x00, 0x00, 0x00,  // offset y
                          0x00, 0x00, 0x00, 0x00   // padding x & padding y

                        };

char STM_Image_Trailer[16] = { 0x00, 0x00, 0x00, 0x00,  // status && Block_ID
                           0x02, 0x00, 0x00, 0x00,  // pkt_type && packet_ID
                           0x00, 0x00, 0x00, 0x01,  // Rsv & Payload type = Image
                           0x00, 0x00, 0x04, 0x00   // size y
                        };





GigeVisionStreaming::GigeVisionStreaming(QObject *parent) :
    QThread(parent)
{


    size_packet_max_payload_ = 8000 ;
    stm_image_payload_ = new char[size_packet_max_payload_+8];

    initialize_ok_ = false;

    lenght_buffer_ = 10;
    image_width_ = 640;
    image_height_ = 480;
//    image_width_ = 1400;
//    image_height_ = 1024;

    display_empty_ = false;

    ChangeResolution(image_width_,image_height_);


    index_last_image_ = 0;



    fps_ = 32;

    fps_display_ = 24;



    // GiGe Vision Streaming Protocol
    to_.sin_family = AF_INET;

    to_.sin_addr.s_addr = htonl(INADDR_ANY);

    to_.sin_port = htons(20000);

    timer_display_ = new QTimer;
    connect(timer_display_,SIGNAL(timeout()),this,SLOT(Display()));

    timer_check_recive_ = new QTimer;
    connect(timer_check_recive_,SIGNAL(timeout()),this,SLOT(CheckReciveImage()));

    thread_is_run_ = false;
    //log_file_thread_ = new LogFile(MODULE_THREADS);

    timer_log_ = new QTimer();
    connect(timer_log_,SIGNAL(timeout()),this,SLOT(SendMessageLog()));

//    FixedConfiguration(type_,loop_,dir_path_);
    //************************************************************************************************

    timer_fps_ = new QTimer;
    fps_period_ = 100;
    nbr_frame_ = 0;
    fps_ = 32;
    connect(timer_fps_,SIGNAL(timeout()),this,SLOT(UpdateFps()));


    InitializeSocket();



}
void GigeVisionStreaming::InitializePortStreaming(QString ip_emission,int port)
{
//    cout<<endl<<"Port UDP Streaming : "<<ip_emission.toStdString()<<"  "<<port<<" OK"<<endl;
    to_.sin_port = htons(port);
    to_.sin_addr.s_addr  = inet_addr(ip_emission.toStdString().c_str());


}

void GigeVisionStreaming::CheckReciveImage()
{
    if(!recive_image_)
    {

        //cout<<endl<<"Redemarage GigeVision  "<<endl;
        Q_EMIT StopAcquireUnexpected();
        framerate_ = 0;
    }
    recive_image_ = false;
}

void GigeVisionStreaming::SendMessageLog()
{
    if(thread_is_run_)
    {
        //log_file_thread_->Information(3);
    }
    else
    {
        //log_file_thread_->Information(4);
    }
    thread_is_run_ = false;
}

bool GigeVisionStreaming::InitializeSocket()
{
    initialize_ok_ = true;
    return true;
}

void GigeVisionStreaming::ChangeIpReception(QString addr)
{

    ip_reception_ =    addr;

    cout<<endl<<"Adresse Ip destination change : "<<ip_reception_.toStdString()<<endl;

    to_.sin_addr.s_addr = inet_addr(ip_reception_.toStdString().c_str());


//    if(!this->isRunning() )
//    {
//        started_ = true;

//        index_last_image_emit_ = -1;
//        index_last_image_ = 0;

//        start(QThread::TimeCriticalPriority);

//        Q_EMIT EnabledRecord(true);

//        timer_fps_->start(fps_period_);
//        if(fps_display_>0)
//            timer_display_->start(1000/fps_display_);
//     }

}

void GigeVisionStreaming::UpdateFps()
{

    Q_EMIT DisplayFps(framerate_);

}


void GigeVisionStreaming::run()
{

    //********************************************************
    QTime new_time = QTime::currentTime();
    QTime last_time = QTime::currentTime();
    double delta_time = 0;
    double total_time = 0;
    int lenght_history = 100;

    QList<double> delta_time_history;

    framerate_ = 0;

    bool start_frame = false;
    bool end_frame = false;

    //********************************************************

    int nbr_oct_recv = 0;

    // Chargement de l'image a  envoyer


    char RecBuff[10000];

    Mat image_read(image_height_,image_width_,CV_8UC1,Scalar(0));
    Mat image_noir(image_height_,image_width_,CV_8UC1,Scalar(0));

    int index_paquet = 0;
    int index_last_paquet = 0;


    int pixel_index=0;

    nbr_frame_error = 0;
    nbr_frame_ = 0;

    bool image_error = false;
    bool image_ok = false;


     SOCKET sclient ;
     socklen_t to_size;
     to_size = sizeof(to_);

     sclient = socket(AF_INET,SOCK_DGRAM, 0);

     if (sclient == INVALID_SOCKET)
     {
         cout<<endl<<"INVALID_SOCKET.... \n"<<endl;
         closesocket(sclient);
         exit(0);
     }

     memset(&to_.sin_zero, '\0', sizeof(to_.sin_zero));
     if (bind(sclient, (SOCKADDR *)&to_, sizeof(to_)) < 0)
     {
         cout<<endl<<"Streaming Stoped erreur bind"<<endl;
         closesocket(sclient);
         QMessageBox::warning(new QWidget,"Erreur Initialisation Module Streaming","Probleme d'initialisation : communication carte reseau impossible...!\nRedemarrer l'application et verifier l'adresse ip de votre machine");
         return ;
     }
     else
     {
         //cout<<endl<<"Initialisation streaming OK"<<endl;
     }

    while(started_)
    {


        start_frame = false;
        end_frame = false;


        nbr_oct_recv = (int) recvfrom(sclient, RecBuff, sizeof(RecBuff) - 1, 0, (SOCKADDR *)&to_, &to_size);


        if (nbr_oct_recv >= 0)
        {
            //Recupperer l'indice du packet:
            index_paquet = 256*256*(uchar)RecBuff[5]+256*(uchar)RecBuff[6]+(uchar)RecBuff[7];

            switch(RecBuff[4])
            {

            start_frame = false;
            end_frame = false;

            case 1:


                // en cas de pert de paquet fin image
                if(pixel_index==0)
                {

                    start_frame = true;

                    // Nouvelle image
                    image_error = false;

                    break;

                }
                //cout<<endl<<"pert de paquet fin image";

            case 2:

                end_frame = true;

                image_ok = false;

                //packet_type_Trailer++;
                if(pixel_index != image_read.rows*image_read.cols || image_error)
                {
                    nbr_frame_error++;
                    //cout<<endl<<"Image recu  "<< nbr_frame_ <<endl;
                    //cout<<endl<<"Image perdu : "<<nbr_frame_error;
                }
                else
                {

                    // Envoi d'un signal au thread d'affichage
                    if(image_read.empty())
                        cout<<endl<<"Image Loading ERROR, size_inc = "<< pixel_index <<"\n"<<endl;
                    else
                    {
                        image_ok = true;
                    }


                }

                // Next image
                index_last_image_ ++;
                if(index_last_image_>=buffer_images_.length())
                    index_last_image_ = 0;

                if(image_ok)
                {
                    image_read.copyTo(buffer_images_[index_last_image_]);
                    Q_EMIT NewImage(&buffer_images_[index_last_image_]);
                }
                else
                {
                    image_noir.copyTo(buffer_images_[index_last_image_]);
                    Q_EMIT NewImage(&buffer_images_[index_last_image_]);


                    if(!display_empty_)
                        index_last_image_emit_ = index_last_image_;
                }



                if(index_last_image_>buffer_images_.length() || index_last_image_<0)
                {
                    cout<<endl<<"Probleme Buffer"<<endl;
                    Stop();
                    closesocket(sclient);
                    return;
                }



                pixel_index = 0;
                image_error = false;

                nbr_frame_++;

                break;

            case 3:

                if(image_read.empty())
                {
                    cout<<endl<<"Probleme Image vide"<<endl;
                    Stop();
                    closesocket(sclient);
                    return;
                }

                //if(index_paquet != index_last_paquet+1)
                 //   cout<<endl<<"Erreur paquet perdu new paquet : "<<index_paquet<<" last paquet : "<<index_last_paquet<<endl;

                //packet_type_Payload++;
                if(!image_error)
                for(int i=8 ; i< nbr_oct_recv ; i++)
                {
                    if(pixel_index < image_read.rows * image_read.cols)
                    {
                        image_read.data[pixel_index] = RecBuff[i];
                        pixel_index++;
                    }
                    else
                        image_error = true;

                }

                break;

            }
            index_last_paquet = index_paquet;
        }




        //************************************************************************
        // calcule fps
        if(start_frame)
        {
            new_time = QTime::currentTime();
        }

        if(end_frame)
        {
            recive_image_ = true;

            delta_time = last_time.msecsTo(new_time);
            last_time = new_time;
            total_time += delta_time;
            delta_time_history.append(delta_time);
            framerate_ = (double)delta_time_history.length()*1000/total_time;

            if(delta_time_history.length()>lenght_history)
            {
                total_time -= delta_time_history.first();
                delta_time_history.removeFirst();
            }

        }
        //************************************************************************

        thread_is_run_ = true;

    }


    closesocket(sclient);

}

void GigeVisionStreaming::Start()
{
    if(!initialize_ok_)
    {
        cout<<endl<<"Initialisation KO...!"<<endl;
        return;
    }

    timer_check_recive_->start(1000);
    cout<<endl<<"Streaming start"<<endl;

    timer_log_->start(1000);
    //log_file_thread_->Information(1);

//return;
    started_ = true;

    index_last_image_emit_ = -1;
    index_last_image_ = 0;

    start(QThread::TimeCriticalPriority);

    Q_EMIT EnabledRecord(true);

    timer_fps_->start(fps_period_);
    if(fps_display_>0)
        timer_display_->start(1000/fps_display_);


}

void GigeVisionStreaming::Stop()
{


    timer_display_->stop();
    timer_check_recive_->stop();
    return;
    Q_EMIT DisplayFps(0);
    started_ = false;
    timer_fps_->stop();
    Q_EMIT EnabledRecord(false);

    timer_log_->stop();
    //log_file_thread_->Information(2);

    cout<<endl<<"Streaming stop"<<endl;
}


 void GigeVisionStreaming::ChangeResolution(int w,int h)
 {
     if(w>0 && h>0)
     {
         image_width_ = w;
         image_height_ = h;

         for(int i=0;i<buffer_images_.length();i++)
         {
             buffer_images_[i].release();
         }
         buffer_images_.clear();

         for(int i=0;i<lenght_buffer_;i++)
         {
             Mat img(image_height_,image_width_,CV_8UC1,Scalar(0));
             buffer_images_.append(img);
         }

     }
     else
         cout<<endl<<"erreur modifiaction resolution"<<endl;
 }

 void  GigeVisionStreaming::Display()
 {
     if(index_last_image_emit_ == index_last_image_)
         return;

     if(index_last_image_ >= buffer_images_.length() || index_last_image_<0)
         return;

     Q_EMIT UpdatedMessage("Streaming : [image recu : "+QString::number(nbr_frame_)+
                           " | image perdu : "+QString::number(nbr_frame_error)+"]");


     if(index_last_image_ >= buffer_images_.length() || index_last_image_ < 0)
         return;

     // Envoi d'un signal au thread d'affichage
     if(buffer_images_[index_last_image_].empty())
         cout<<endl<<"Image Loading ERROR"<<endl;
     else
     {
//         imshow("Test",buffer_images_[index_last_image_]);
         Q_EMIT NewImageDisplay(&buffer_images_[index_last_image_]);

     }

     index_last_image_emit_ = index_last_image_;
 }

 GigeVisionStreaming::~GigeVisionStreaming()
{
     // WSAStartup
#ifdef WIN32
     WSACleanup();
#endif

     for(int i=0;i<buffer_images_.length();i++)
         buffer_images_[i].release();
}
