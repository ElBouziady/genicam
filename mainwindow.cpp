#include "mainwindow.h"
#include "ui_mainwindow.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    genicam_ = new AcquireCamera();

    //QObject::connect(genicam_, SIGNAL(ImageAvailable(Mat*)),buffer_, SLOT(AddFrame(Mat*)));
    //QObject::connect(genicam_, SIGNAL(Pause()),buffer_, SLOT(Pause()));

    //genicam_->StartAquire();
    //genicam_->StopAquire();

}

MainWindow::~MainWindow()
{
    delete genicam_;
    delete ui;
}
