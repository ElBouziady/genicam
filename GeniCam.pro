#-------------------------------------------------
#
# Project created by QtCreator 2018-01-31T16:07:39
#
#-------------------------------------------------

QT       += core gui multimedia multimediawidgets xml serialport network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = GeniCam
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    Acquire/acquire_camera.cpp \
    Acquire/GeniCamTypes.cpp \
    Acquire/gigevision_control.cpp \
    Acquire/gigevision_streaming.cpp \
    Acquire/serveur_gigevision.cpp \
    Acquire/TransportLayer.cpp \

HEADERS  += mainwindow.h \
    Acquire/acquire_camera.h \
    Acquire/GeniCamTypes.h \
    Acquire/gigevision_control.h \
    Acquire/gigevision_streaming.h \
    Acquire/serveur_gigevision.h \
    Acquire/structs.h \
    Acquire/TransportLayer.h \

FORMS    += mainwindow.ui \
    Acquire/acquire_camera.ui \

###########################################################################################################
#                             IDS                                                                   ###
###########################################################################################################

!win32 {
    # Linux
    DEFINES += __LINUX__
    LIBS += -lueye_api
} else {
    # Windows
    !contains(QMAKE_TARGET.arch, x86_64) {
        LIBS += -lueye_api
    } else {
        LIBS += -lueye_api_64
    }
}

INCLUDEPATH += \
    . \
    include \
    src


###########################################################################################################
#                             OpenCV                                                                   ###
###########################################################################################################

unix {
LIBS += `pkg-config --libs opencv`
LIBS+= -L"/usr/local/lib/"  -lopencv_highgui

INCLUDEPATH += '/usr/local/include'

}


###########################################################################################################
#                             GenICam                                                                   ###
###########################################################################################################

win32{
INCLUDEPATH += "C:\Program Files\GenICam_v2_4\library\CPP\include"

LIBS        += -L"C:/Program Files/GenICam_v2_4/library/CPP/lib/Win32_i86/"         -lCLAllSerial_MD_VC100_v2_4 \
                                                                                    -lCLProtocol_MD_VC100_v2_4 \
                                                                                    -lCLSerCOM \
                                                                                    -lGCBase_MD_VC100_v2_4 \
                                                                                    -lGenApi_MD_VC100_v2_4 \
                                                                                    -lGenCP_MD_VC100_v2_4 \
                                                                                    -lLog_MD_VC100_v2_4 \
                                                                                    -llog4cpp_MD_VC100_v2_4 \
                                                                                    -llog4cpp-static_MD_VC100_v2_4 \
                                                                                    -lMathParser_MD_VC100_v2_4
}

unix{

INCLUDEPATH += "/usr/local/genicam/library/CPP/include"
LIBS        += -L"/usr/local/genicam/library/CPP/lib/Linux64_x64/"          -llog4cpp-static_gcc40_v2_4


LIBS        += -L"/usr/local/genicam/bin/Linux64_x64/"                      -lGCBase_gcc40_v2_4 \
                                                                            -lGenApi_gcc40_v2_4 \
                                                                            -lLog_gcc40_v2_4 \
                                                                            -llog4cpp_gcc40_v2_4 \
                                                                            -lMathParser_gcc40_v2_4

LIBS        += -L"/usr/local/genicam/bin/Linux64_x64/GenApi/Generic"        -lXalan-C_gcc40_v1_10_1 \
                                                                            -lXalanMessages_gcc40_v1_10_1 \
                                                                            -lXerces-C_gcc40_v2_7_1 \
                                                                            -lXMLLoader_gcc40_v2_4


GENICAM_ROOT_V2_4 = /usr/local/genicam

}
